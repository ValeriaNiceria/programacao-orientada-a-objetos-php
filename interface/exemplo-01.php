<?php

interface Veiculo{

    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($marcha);

}


class Civic implements Veiculo{

    public function acelerar($velocidade){
        return "O veículo acelerou até " . $velocidade . "km/h";
    }
    
    public function frenar($velocidade){
        return "O veículo frenou até atingir a velocidade de " . $velocidade . "km/h";
    }

    public function trocarMarcha($marcha){
        return "O veículo engatou a marcha " . $marcha;
    }

}


$carro = new Civic;

echo ($carro->acelerar(100));
echo "<br/>";

echo ($carro->frenar(20));
echo "<br/>";

echo ($carro->trocarMarcha(3));
?>  