<?php

class Carro{
    private $modelo;
    private $motor;
    private $ano;

    public function getModelo(){
        return $this->modelo;
    }
    public function setModelo($value){
        $this->modelo = $value;
    }

    public function getMotor():float{
        return $this->motor;
    }
    public function setMotor($value){
        $this->motor = $value;
    }

    public function getAno():int{
        return $this->ano;
    }
    public function setAno($value){
        $this->ano = $value;
    }

    public function exibir(){
        return array(
            "Modelo"=>$this->getModelo(),
            "Motor"=>$this->getMotor(),
            "Ano"=>$this->getAno()
        );
    }
}

//Intanciando a classe Carro

$palio = new Carro();

$palio->setModelo("Adventure");
$palio->setMotor("1.6");
$palio->setAno("2000");

var_dump($palio->exibir());

?> 