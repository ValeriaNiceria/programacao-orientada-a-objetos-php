<?php

abstract class Animal{

    public function falar(){
        return "Som do animal!";
    }

    public function mover(){
        return "Anda!";
    }
}

class Cachorro extends Animal{
    public function falar(){
        return "AU AU AU!";
    }
}

class Passaro extends Animal{
    public function mover(){
        return "Voa e " . parent::mover();
    }

}

class Gato extends Animal{
    public function falar(){
        return "Miaauuu!";
    }
}

$pluto = new Cachorro();
echo $pluto->falar() . "<br/>";


$bird = new Passaro();
echo $bird->mover() . "<br/>";

$cat = new Gato();
echo $cat->falar();

?> 