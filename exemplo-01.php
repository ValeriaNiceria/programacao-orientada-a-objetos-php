<?php

class Pessoa{
    public $nome; //Atributo

    public function falar(){ //Método
        return "O meu nome é ".$this->nome;
    }
}

$catarina = new Pessoa();
$catarina->nome = "Catarina Martins";
echo $catarina->falar();


?> 