<?php

abstract class Automovel implements Veiculo{
    
        public function acelerar($velocidade){
            return "O veículo acelerou até " . $velocidade . "km/h";
        }
        
        public function frenar($velocidade){
            return "O veículo frenou até atingir a velocidade de " . $velocidade . "km/h";
        }
    
        public function trocarMarcha($marcha){
            return "O veículo engatou a marcha " . $marcha;
        }
    
    }

?>