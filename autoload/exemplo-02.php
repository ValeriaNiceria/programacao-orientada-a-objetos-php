<?php

    //Funções SPL
    //spl_autoload_register -> Registra a função dada como implementação de __autoload()

    function incluirClasses($nomeClasse){
        if(file_exists($nomeClasse.".php")){
            require_once("{$nomeClasse}.php");
        }
    }

    spl_autoload_register("incluirClasses");
    spl_autoload_register(function($nomeClasse){
        if(file_exists("class/{$nomeClasse}.php")){
            require_once("class/{$nomeClasse}.php");
        }
    });


    $gol = new Gol();
    echo $gol->frenar(30);
?>