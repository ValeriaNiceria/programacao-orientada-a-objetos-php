<?php

class Documento{
    private $numero;

    public function getNumero(){
        return $this->numero;
    }
    public function setNumero($value){
        $this->numero = $value;
    }
}

//Herança
class Cpf extends Documento{
    public function validar():bool{

        $numeroCpf = $this->getNumero();
        
        //Código de validação do CPF

        return true;
    }

}

//Instanciando
$doc = new Cpf();

$doc->setNumero("111109987521");

var_dump($doc->validar());

echo "<br/>";

echo ($doc->getNumero());

?>